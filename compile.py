import os
import sys
import platform
import shutil
smee2_qt = "./smee2_py/dist/smee2_qt"
if os.path.exists(smee2_qt):
	shutil.rmtree(smee2_qt)

#server
os.system("cargo b -r --bin smee2_server")

#gui
os.chdir("./smee2_py")
os.system("pip install maturin pyside6 pyinstaller")
os.system("maturin develop -r")

#compile .ui
def compile_ui():
	print("compiling .ui files to python")
	if not os.path.exists("./qt_lib"):
		os.mkdir("./qt_lib")
	files = []
	for i in os.listdir("./ui"):
		if i.endswith(".ui"):
			files.append(i)
	for input in files:
		output = input.replace(".ui", ".py")
		os.system(f"pyside6-uic ./ui/{input} -o ./qt_lib/{output}")

compile_ui()

os.system("pyinstaller smee2_qt.py")
os.chdir("../")

match platform.system():
	case "Windows":
		server = "./builds/smee2_server/windows/"
		if os.path.exists(server):
			shutil.rmtree(server)
		os.makedirs(server)
		gui = "./builds/smee2_py/windows/"
		if os.path.exists(gui):
			shutil.rmtree(gui)
		os.makedirs(gui)

		shutil.move("./target/release/smee2_server.exe", "./builds/smee2_server/windows/smee2_server.exe")
		shutil.move(smee2_qt, "./builds/smee2_py/windows/")
	case "Linux":
		server = "./builds/smee2_server/Linux/"
		if os.path.exists(server):
			shutil.rmtree(server)
		os.makedirs(server)
		gui = "./builds/smee2_py/Linux/"
		if os.path.exists(gui):
			shutil.rmtree(gui)
		os.makedirs(gui)

		shutil.move("./target/release/smee2_server", "./builds/smee2_server/Linux/smee2_server")
		shutil.move(smee2_qt, "./builds/smee2_py/Linux/")



#cleanup
shutil.rmtree("./smee2_py/build")
shutil.rmtree("./smee2_py/dist")
os.remove("./smee2_py/smee2_qt.spec")