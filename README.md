# Smee2

***Save me, Smee!***

Smee2 is a save game manager, it creates backups of your saves and uploads them to the server. Currently only windows is supported, but linux support should arrive soon

## Contents

- [Screenshots](#screenshots)
- [Quick start guide](#quick-start-guide)
- [Wiki](#wiki)
	- [Compiling](#compiling)
 
## Screenshots

### Main

![Image](/screenshots/main.png)

### Push

![Image](/screenshots/push.png)

### Pull

![Image](/screenshots/pull.png)

## Quick start guide

The first thing to set up is the server, creatively name smee2_server.
To setup, run `smee2_server -c` to make the configuration file
### Server.toml

---

``` toml
source = "./server-data"
key = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
port = "42069"
```

`source` refers to an empty directory for the server to use to store the save data in.

`key` is the encryption key to use with the client for communications.

`port` refers to the port used by the server.

---

Next comes the GUI

![Image](/screenshots/config.png)

The first thing you see is the config creation tool.

`Path to profiles` refers to the path to an empty directory, used by the gui to keep configurations.

`Encryption key` is the same as for the server. If the server and client keys do not match, it won't work. 

`Server Address` refers to the IP address of the smee2 server. In form 192.168.20.47:42069

after pressing `Make Config`, it will create a `config.toml` file, containing the information you entered in the same directory as the executable.

now you are setup and ready to be saved by Smee.

---

## Wiki

the Smee2 protocol has several functions.

- `Push`: Uploads a save game, all of it, all the directories and files associated with that game according to the user configurations

- `Pull`: Downloads a previously pushed save game. It replaces the contents of the files and directories used.

- `Fetch`: is used to ask what saves the server has for a particular game.

- `Fetch Games`: is used to ask what games the server has saves for.

### Compiling

#### Prerequisites

- Rustup
- Python (3.11 recommended) with pip and venv

Rustup needs to be installed to compile the server and the libraries, the python QT6 GUI is compiled with pyinstaller, all is done with the simple compile script.

compiling in venv is recommended:

``` cmd
python -m venv .env
.env\Scripts\activate.bat
python compile.py
```

or on GNU/linux:

``` bash
python3 -m venv .env
source .env/bin/activate
python3 compile.py
```
