#![allow(non_camel_case_types)]
use std::collections::HashMap;

use pyo3::prelude::*;
use smee2_client::{Config, transfer::{fetch_games, pull, push, fetch, fetch_save_location_names}};
use smee2_lib::{game_manager::{GameSave, GameProfile}, transfer::GameSavePull};
#[pyclass]
#[derive(Clone)]
struct pyConfig(Config);

#[pyfunction]
fn pyconfig_get_profile_path(config: pyConfig) -> String{
    config.0.profile_path
}

#[pyfunction]
fn pyconfig_export(config: pyConfig, config_file: String) {
    config.0.export_config(config_file);
}

#[pyfunction]
fn pyconfig_from_string(config_str: &str) -> pyConfig {
    pyConfig(Config::toml_from_str(config_str))
}

#[pyfunction]
fn pyconfig_new(profile_path: String, key_str: String, server_address: String) -> pyConfig {
    pyConfig(Config::new(profile_path, key_str, server_address))
}

#[pyclass]
#[derive(Clone)]
struct pyGameSave(GameSave);

#[pyfunction]
fn backup(profile: pyGameProfile, save_name: String) -> PyResult<pyGameSave> {
    let time = smee2_client::current_time();
    match smee2_client::game_profile::backup(profile.0, save_name, time) {
        Ok(save) => {
            Ok(pyGameSave(save))
        },
        Err(e) => {
            println!("Backup Error {}", e);
            Err(PyErr::from(e))
        }
    }
}

#[pyfunction]
fn py_push(save: pyGameSave, config: pyConfig) -> PyResult<()> {
    if let Err(e) = push(&save.0, &config.0.server_address, &config.0.key) {
        Err(PyErr::from(e))
    } else {
        Ok(())
    }
}

#[pyclass]
#[derive(Clone)]
struct pyGameSavePull(GameSavePull);

#[pyfunction]
fn py_pull_create(game_name: String, save_name: String) -> pyGameSavePull {
    pyGameSavePull(
        GameSavePull {
            game_name: game_name,
            save_name: smee2_lib::transfer::SaveType::Specific(save_name)
        }
    )
}

#[pyfunction]
fn py_pull(game_save: pyGameSavePull, config: pyConfig, profile: self::pyGameProfile){
    match pull(&config.0.server_address, &config.0.key, game_save.0) {
        Ok(game_save) => {
            if let Err(e) = smee2_client::game_profile::restore(&profile.0, game_save) {
                println!("Can't restored pulled save: {}", e);
            }
        },
        Err(e) => {
            println!("Error pulling {}", e);
        }
    }
}
#[pyfunction]
fn find_profile(name: String, profiles: Vec<pyGameProfile>) -> pyGameProfile {
    let profiles: Vec<pyGameProfile> = profiles.into_iter().filter(|x| x.0.name == name).collect();
    profiles[0].clone()
}

#[pyfunction]
fn py_fetch(config: pyConfig, game_name: String) -> Vec<String> {
    match fetch(&config.0.server_address, &config.0.key, game_name.clone()) {
        Ok(saves) => {
            saves
        },
        Err(e) => {
            println!("Can't fetch saves of {game_name}\n{}", e);
            Vec::new()
        }
    }
}

#[pyfunction]
fn py_fetch_games(config: pyConfig) -> PyResult<Vec<String>> {
    match fetch_games(&config.0.server_address, &config.0.key) {
        Ok(games) => {
            Ok(games)
        },
        Err(e) => {
            Err(PyErr::from(e))
        }
    }
}

#[pyfunction]
fn py_fetch_save_locations(game_save: pyGameSavePull, config: pyConfig) -> Vec<String> {
    match fetch_save_location_names(&config.0.server_address, &config.0.key, game_save.0) {
        Ok(game_save) => {
            game_save
        },
        Err(e) => {
            
            println!("Error pulling {}", e);
            return Vec::<String>::new();
        }
    }
}

#[pyclass]
#[derive(Clone)]
struct pyGameProfile(GameProfile);

fn is_dir(path: &String) -> Result<bool, std::io::Error>{
    match std::path::Path::new(&path).metadata() {
        Ok(n) => {
            Ok(n.is_dir())
        },
        Err(e) => {
            Err(e)
        }
    }
}

//to replace py game profile new, used in qt
#[pyfunction]
fn py_game_profile_make(name: String, save_locations: Vec<(String, String)>) -> Result<pyGameProfile, std::io::Error> {
    let mut saves = HashMap::new();
    for save in save_locations {
        match is_dir(&save.1) {
            Ok(n) => {
                saves.insert(save.0, (save.1, n));
            },
            Err(e) => {
                println!("failes to make game profile {}", e);
                return Err(e);
            }
        }
    }
    let profile = GameProfile {
        name: name,
        saves: saves
    };
    Ok(pyGameProfile(profile))
}

#[pyfunction]
fn py_game_profile_new(name: String, save_location_name: String, save_location: String, isdir: bool) -> pyGameProfile {
    let mut saves = HashMap::new();
    saves.insert(save_location_name, (save_location, isdir));
    let profile = GameProfile {
        name: name,
        saves: saves
    };
    pyGameProfile(profile)
}

#[pyfunction]
fn create_game_profile(dir_path: String, profile: pyGameProfile) -> bool {
    match smee2_client::game_profile::create_profile(dir_path, profile.0) {
        Ok(()) => {
            true
        },
        Err(e) => {
            println!("python create game profile error {}", e);
            false
        }
    }
}

#[pyfunction]
fn get_profiles(dir_path: String) -> Vec<pyGameProfile> {
    match smee2_client::game_profile::get_profiles(dir_path) {
        Err(e) => {
            println!("Errors Reading directory {}", e);
            Vec::new()
        },
        Ok((profiles, errs)) => {
            if errs.len() > 0 {
                println!("Errors parsing toml files {:#?}", errs)
            }
            let profiles: Vec<pyGameProfile> = profiles.into_iter().map(|x| pyGameProfile(x)).collect();
            profiles
        }
    }

}

#[pyfunction]
fn get_profile_names(profiles: Vec<pyGameProfile>) -> Vec<String> {
    profiles.into_iter().map(|x| x.0.name).collect()
}
// for qt
#[pyfunction]                                       // game name, save names, save locations
fn get_profile_info(profiles: Vec<pyGameProfile>) -> Vec<(String, Vec<String>, Vec<String>)> {
    let mut info: Vec<(String, Vec<String>, Vec<String>)> = Vec::new();
    for i in 0..profiles.len() {
        let name = profiles[i].0.name.clone();
        let mut save_names: Vec<String> = Vec::new();
        let mut save_paths: Vec<String> = Vec::new();
        for save in &profiles[i].0.saves {
            let save_name = save.0.clone();
            let save_path = save.1.0.clone();
            save_names.push(save_name);
            save_paths.push(save_path);
        }
        info.push((name, save_names, save_paths))
    }
    info
}

#[pyfunction]
fn append_profile(mut profile: pyGameProfile, source_name: String, source_path: String) -> pyGameProfile{
    let is_dir = is_dir(&source_path).unwrap();
    profile.0.saves.insert(source_name, (source_path, is_dir));
    profile
}

#[pyfunction]
fn remove_profile_source(mut input: pyGameProfile, key: String) -> pyGameProfile {
    input.0.saves.remove(&key);
    input
}

/// A Python module implemented in Rust. The name of this function must match
/// the `lib.name` setting in the `Cargo.toml`, else Python will not be able to
/// import the module.
#[pymodule]
fn smee2_py(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(py_push, m)?)?;
    m.add_function(wrap_pyfunction!(py_pull, m)?)?;
    m.add_function(wrap_pyfunction!(py_fetch_games, m)?)?;
    m.add_function(wrap_pyfunction!(pyconfig_from_string, m)?)?;
    m.add_function(wrap_pyfunction!(pyconfig_new, m)?)?;
    m.add_function(wrap_pyfunction!(py_game_profile_new, m)?)?;
    m.add_function(wrap_pyfunction!(create_game_profile, m)?)?;
    m.add_function(wrap_pyfunction!(get_profiles, m)?)?;
    m.add_function(wrap_pyfunction!(get_profile_names, m)?)?;
    m.add_function(wrap_pyfunction!(backup, m)?)?;
    m.add_function(wrap_pyfunction!(py_fetch, m)?)?;
    m.add_function(wrap_pyfunction!(py_pull_create, m)?)?;
    m.add_function(wrap_pyfunction!(pyconfig_get_profile_path, m)?)?;
    m.add_function(wrap_pyfunction!(pyconfig_export, m)?)?;
    m.add_function(wrap_pyfunction!(find_profile, m)?)?;
    m.add_function(wrap_pyfunction!(py_game_profile_make, m)?)?;
    m.add_function(wrap_pyfunction!(get_profile_info, m)?)?;
    m.add_function(wrap_pyfunction!(append_profile, m)?)?;
    m.add_function(wrap_pyfunction!(remove_profile_source, m)?)?;
    m.add_function(wrap_pyfunction!(py_fetch_save_locations, m)?)?;
    m.add_class::<pyConfig>()?;
    m.add_class::<pyGameSave>()?;
    m.add_class::<pyGameSavePull>()?;
    m.add_class::<pyGameProfile>()?;
    Ok(())
}