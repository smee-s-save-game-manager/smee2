import os
import smee2_py
import PySimpleGUI as gui

if not os.path.exists("./config.toml"):
	layout = [
		[gui.Text("path to profiles"), gui.InputText(key="path_profiles")],
		[gui.Text("encryption key"), gui.InputText(key="encryption_key")],
		[gui.Text("Server address"), gui.InputText(key="server_address")],
		[gui.Button("Create Config")]
	]
	window_config = gui.Window("Smee2 Py GUI", layout, finalize=True)
	while True:
		event, values = window_config.read()
		if event == "Create Config":
			config = smee2_py.pyconfig_new(values["path_profiles"], values["encryption_key"], values["server_address"])
			smee2_py.pyconfig_export(config, "./config.toml")
			window_config.close()
			break

config_file = open("./config.toml", "r")
config = smee2_py.pyconfig_from_string(config_file.read())

profiles_path = smee2_py.pyconfig_get_profile_path(config)
if not os.path.exists(profiles_path):
    os.mkdir(profiles_path)
profiles = smee2_py.get_profiles(profiles_path)

def make_main_window():
	layout = [
	[gui.Text("", key="game list")], 
	[gui.Button("Push")],
	[gui.Button("Pull")],
	[gui.Button("Fetch Games")],
	[gui.Button("Add Games")],
	[gui.Button("Edit Game Profile")],
	]
	return gui.Window("Smee2 Py GUI", layout, finalize=True)

def make_add_game_window():
	games_layout = [
	[gui.Text("Game Name"), gui.InputText()],
	[gui.Text("Save Location Name"), gui.InputText()],
	[gui.Text("Save Location"), gui.InputText()],
	[gui.Checkbox("is directory?")],
	[gui.Button("Add Game")],
	]
	return gui.Window("Smee2 Py GUI", games_layout, finalize=True)
	
def make_push_window():
	push_layout = [
		[gui.Text("Push")],
		[gui.Combo(smee2_py.get_profile_names(profiles), key="upload game name")],
		[gui.Text("Save Name:"), gui.InputText(key="upload save name")],
		[gui.Button("Push Game")]
	]
	return gui.Window("Smee2 Py GUI", push_layout, finalize=True)

def make_pull_window():
	pull_layout = [
		[gui.Text("Pull")],
		[gui.Combo(smee2_py.get_profile_names(profiles), key="game pull selection")],
		[gui.Button("Fetch Saves"), gui.Combo(["latest"], key="saves")],
		[gui.Button("Pull Save")]
	]
	return gui.Window("Smee2 Py GUI", pull_layout, finalize=True)

def make_edit_profile_window():
	layout = [
	[gui.Combo(values=smee2_py.get_profile_names(profiles))],
	[gui.Text("Save Location Name"), gui.InputText()],
	[gui.Text("Save Location"), gui.InputText()],
	[gui.Checkbox("is directory?")],
	[gui.Button("Edit Game")],
	]
	return gui.Window("Smee2 Py GUI", layout, finalize=True)

main_window = make_main_window()

while True:
	window, event, values = gui.read_all_windows()
	match event:
		case "Push":
			window_push = make_push_window()
		case "Pull":
			window_pull = make_pull_window()
		case "Fetch Games":
			window["game list"].update(smee2_py.py_fetch_games(config))
		case "Add Games":
			window_add_games = make_add_game_window()
		case "Add Game":
			profile = smee2_py.py_game_profile_new(values[0], values[1], values[2], values[3])
			smee2_py.create_game_profile(profiles_path, profile)
			profiles.append(profile)
			window_add_games.close()
		case "Push Game":
			if values["upload save name"] and values["upload game name"]:
				backup = smee2_py.backup(smee2_py.find_profile(values["upload game name"], profiles), values["upload save name"])
				smee2_py.py_push(backup, config)
				window_push.close()
		case "Pull Save":
			name = values["game pull selection"]
			save = values["saves"]
			if name and save:
				_pull = smee2_py.py_pull_create(name, save)
				profile_ = smee2_py.find_profile(name, profiles)
				smee2_py.py_pull(_pull, config, profile_)
				window_pull.close()
		case "Fetch Saves":
			saves = smee2_py.py_fetch(config, values["game pull selection"])
			window_pull["saves"].update(value=saves[0], values=saves)
		case "Edit Game Profile":
			edit_profile_window = make_edit_profile_window()
		case "Edit Game":
			print(values)
			profile = smee2_py.find_profile(values[0], profiles)
			profile = smee2_py.append_profile(profile, values[1], values[2], values[3])
			if not smee2_py.create_game_profile(profiles_path, profile):
				print("can't edit")
			edit_profile_window.close()
	if event == gui.WIN_CLOSED:
		if window != main_window:
			window.close()
		else:
			break

window.close()
