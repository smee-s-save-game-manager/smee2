import sys
import os
import random
from PySide6 import QtCore, QtWidgets, QtGui
import smee2_py
from qt_lib.main import Ui_MainWindow
from qt_lib.add_games_window import Ui_Add_game_win
from qt_lib.add_games_add_path import Ui_add_game_add_path_win
from qt_lib.push_window import Ui_PushDialog
from qt_lib.pull_window import Ui_PullDialog
from qt_lib.config_maker import Ui_ConfigMaker
from qt_lib.edit_profile import Ui_EditWindow
from qt_lib.info import Ui_InfoWindow
profiles = []
class AddSourceWin(QtWidgets.QDialog):
	new_source = QtCore.Signal(str, str)
	def __init__(self):
		super().__init__()
		self.ui = Ui_add_game_add_path_win()
		self.ui.setupUi(self)
		self.ui.FileSelectBtn.clicked.connect(self.file_select_dialogue)
		self.ui.DirSelectBtn.clicked.connect(self.dir_select_dialogue)

	def dir_select_dialogue(self):
		dlg = QtWidgets.QFileDialog()
		dlg.setFileMode(QtWidgets.QFileDialog.Directory)
		dlg.fileSelected.connect(self.selected)
		dlg.exec()

	def file_select_dialogue(self):
		dlg = QtWidgets.QFileDialog()
		dlg.setFileMode(QtWidgets.QFileDialog.AnyFile)
		dlg.fileSelected.connect(self.selected)
		dlg.exec()

	def selected(self, file):
		self.ui.SaveLocationInput.setText(file)

	def accept(self):
		name = self.ui.SaveLocationNameInput.text()
		path = self.ui.SaveLocationInput.text()
		if name != "" and path != "":
			self.new_source.emit(name, path)
			self.done(0)

class AddGameWindow(QtWidgets.QDialog):
	sources = []
	def __init__(self):
		super().__init__()
		self.ui = Ui_Add_game_win()
		self.ui.setupUi(self)
		self.ui.sources.setHeaderLabel("sources")
		self.ui.AddSourceBtn.clicked.connect(self.add_source_win)

	def new_source(self, name, path):
		item = QtWidgets.QTreeWidgetItem([name])
		item.addChild(QtWidgets.QTreeWidgetItem([path]))
		self.ui.sources.insertTopLevelItem(0, item)
		self.sources.append((name, path))

	def add_source_win(self):
		add_source_win = AddSourceWin()
		add_source_win.new_source.connect(self.new_source)
		add_source_win.exec()

	def accept(self):
		#todo error handling
		game_profile = smee2_py.py_game_profile_make(self.ui.GameNameInput.text(), self.sources)
		if game_profile:
			smee2_py.create_game_profile(profiles_path, game_profile)
			profiles.append(game_profile)

class EditGameWindow(QtWidgets.QDialog):
	current_profile = 0
	new_profiles = []
	def __init__(self):
		super().__init__()
		self.ui = Ui_EditWindow()
		self.ui.setupUi(self)
		self.ui.GamesCombo.addItems(smee2_py.get_profile_names(profiles))
		self.ui.GamesCombo.activated.connect(self.choose_game)
		self.choose_game(0)
		self.ui.sources.setHeaderLabel("sources")
		self.ui.AddBtn.clicked.connect(self.add_source_win)
		self.ui.DelBtn.clicked.connect(self.remove_source)

	def remove_source(self):
		new_profile = profiles[self.current_profile]
		for source in self.ui.sources.selectedItems():
			print(source.data(0, 0))
			new_profile = smee2_py.remove_profile_source(new_profile, source.data(0, 0))
		self.new_profiles.append((self.current_profile, new_profile))
		self.choose_game(self.ui.GamesCombo.currentIndex())

	def choose_game(self, index):
		self.ui.sources.clear()
		[(game_name, save_names, save_paths)] = smee2_py.get_profile_info([self.update_profiles(False)[index]])
		items = []
		for i in range(0, len(save_names)):
			item = QtWidgets.QTreeWidgetItem([save_names[i]])
			item.addChild(QtWidgets.QTreeWidgetItem([save_paths[i]]))
			items.append(item)
		self.ui.sources.insertTopLevelItems(0, items)
		self.current_profile = index

	def new_source(self, name, path):
		self.new_profiles.append((self.current_profile, smee2_py.append_profile(profiles[self.current_profile], name, path)))
		item = QtWidgets.QTreeWidgetItem([name])
		item.addChild(QtWidgets.QTreeWidgetItem([path]))
		self.ui.sources.insertTopLevelItem(0, item)

	def add_source_win(self):
		add_source_win = AddSourceWin()
		add_source_win.new_source.connect(self.new_source)
		add_source_win.exec()

	def accept(self):
		self.update_profiles(True)
		self.done(0)

	def reject(self):
		self.new_profiles.clear()
		self.done(0)

	def update_profiles(self, write=False):
		global profiles
		updated = profiles.copy()
		for (index, profile) in self.new_profiles:
			updated[index] = profile
			if write:
				smee2_py.create_game_profile(profiles_path, profile)
		if write:
			profiles = updated
		else:
			return updated

class PushWindow(QtWidgets.QDialog):
	def __init__(self):
		super().__init__()
		self.ui = Ui_PushDialog()
		self.ui.setupUi(self)
		self.ui.GameCombo.addItems(smee2_py.get_profile_names(profiles))
		self.ui.PushBtn.clicked.connect(self.push)
	def push(self):
		save = self.ui.SaveNameInput.text()
		current_profile = smee2_py.find_profile(self.ui.GameCombo.currentText(), profiles)
		if save != "" and save != "latest" and current_profile:
			backup = smee2_py.backup(current_profile, save)
			smee2_py.py_push(backup, config)
			self.done(0)

class PullWindow(QtWidgets.QDialog):
	def __init__(self):
		super().__init__()
		self.ui = Ui_PullDialog()
		self.ui.setupUi(self)
		self.ui.GameCombo.addItems(smee2_py.get_profile_names(profiles))
		self.ui.GameCombo.activated.connect(self.get_saves)
		self.ui.PullBtn.clicked.connect(self.pull)
	def pull(self):
		game_name = self.ui.GameCombo.currentText()
		save_name = self.ui.SaveCombo.currentText()
		if game_name != "" and save_name != "":
			pull = smee2_py.py_pull_create(game_name, save_name)
			profile = smee2_py.find_profile(game_name, profiles)
			smee2_py.py_pull(pull, config, profile)
			self.done(0)

	def get_saves(self, index):
		game_name = self.ui.GameCombo.itemText(index)
		saves = smee2_py.py_fetch(config, game_name)
		self.ui.SaveCombo.clear()
		if saves:
			self.ui.SaveCombo.addItems(saves)

class InfoWinow(QtWidgets.QDialog):
	def __init__(self):
		super().__init__()
		self.ui = Ui_InfoWindow()
		self.ui.setupUi(self)

class MainWindow(QtWidgets.QWidget):
	def __init__(self):
		super().__init__()
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.ui.AddGameBtn.clicked.connect(self.add_game_win)
		self.ui.PushBtn.clicked.connect(self.push_win)
		self.ui.PullBtn.clicked.connect(self.pull_win)
		self.ui.EditGameBtn.clicked.connect(self.edit_game_win)
		self.ui.InfoBtn.clicked.connect(self.info_win)
		self.ui.RefreshBtn.clicked.connect(self.refresh)
		self.update_games()

	def refresh(self):
		global profiles
		profiles = smee2_py.get_profiles(profiles_path)
		self.update_games()

	def update_games(self):
		try:
			if len(profiles) != 0:
				items = []
				for (name, saves, save_paths) in smee2_py.get_profile_info(profiles):
					item = QtWidgets.QTreeWidgetItem([name])
					for x in range(0, len(saves)):
						save = saves[x]
						path = save_paths[x]
						save = QtWidgets.QTreeWidgetItem([save])
						save.addChild(QtWidgets.QTreeWidgetItem([path]))
						item.addChild(save)
					items.append(item)
				self.ui.ProfilesTree.clear()
				self.ui.ProfilesTree.addTopLevelItems(items)

			games = smee2_py.py_fetch_games(config)
			if games:
				items = []
				for game in games:
					saves = smee2_py.py_fetch(config, game)
					for save in saves:
						save_request = smee2_py.py_pull_create(game, save)
						save_locations = smee2_py.py_fetch_save_locations(save_request, config)
					item = QtWidgets.QTreeWidgetItem([game])
					for save in saves:
						save_widget = QtWidgets.QTreeWidgetItem([save])
						for location in save_locations:
							save_widget.addChild(QtWidgets.QTreeWidgetItem([location]))
						item.addChild(save_widget)
					items.append(item)
				self.ui.UploadsTree.clear()
				self.ui.UploadsTree.addTopLevelItems(items)
		except:
			print("Can't connect to Smee2 Server")

	def add_game_win(self):
		add_game_win = AddGameWindow()
		add_game_win.exec()
	def edit_game_win(self):
		edit_win = EditGameWindow()
		edit_win.exec()
	def push_win(self):
		push_win = PushWindow()
		push_win.exec()
	def pull_win(self):
		pull_win = PullWindow()
		pull_win.exec()
	def info_win(self):
		info = InfoWinow()
		info.exec()

class ConfigMaker(QtWidgets.QDialog):
	def __init__(self):
		super().__init__()
		self.ui = Ui_ConfigMaker()
		self.ui.setupUi(self)
		self.ui.ConfigBtn.clicked.connect(self.create_config)
		self.ui.FileSelectBtn.clicked.connect(self.select_dir)
	
	def select_dir(self):
		dlg = QtWidgets.QFileDialog()
		dlg.setFileMode(QtWidgets.QFileDialog.Directory)
		dlg.fileSelected.connect(self.selected)
		dlg.exec()

	def selected(self, file):
		self.ui.PathInput.setText(file)
	def create_config(self):
		path_profiles = self.ui.PathInput.text()
		encryption_key = self.ui.KeyInput.text()
		server_address = self.ui.AddressInput.text()
		config = smee2_py.pyconfig_new(path_profiles, encryption_key, server_address)
		smee2_py.pyconfig_export(config, "./config.toml")
		self.done(0)

if __name__ == "__main__":
	app = QtWidgets.QApplication([])
	if not os.path.exists("./config.toml"):
		window = ConfigMaker()
		window.exec()
	config_file = open("./config.toml", "r")
	config = smee2_py.pyconfig_from_string(config_file.read())

	profiles_path = smee2_py.pyconfig_get_profile_path(config)
	if not os.path.exists(profiles_path):
		os.mkdir(profiles_path)

	profiles = smee2_py.get_profiles(profiles_path)

	
	widget = MainWindow()
	widget.show()

	sys.exit(app.exec())