# to compile all the *.ui files to python
import os

def compile():
	print("compiling .ui files to python")
	files = []
	os.mkdir("./qt_lib")
	for i in os.listdir("./ui"):
		if i.endswith(".ui"):
			files.append(i)

	for input in files:
		output = input.replace(".ui", ".py")
		os.system(f"pyside6-uic ./ui/{input} -o ./qt_lib/{output}")

compile()