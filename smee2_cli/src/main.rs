#![allow(dead_code)]
mod cli;
use smee2_client::Config;
fn main() {
    let config = Config::import_config("./config.toml".to_string()).unwrap_or_else( || {
        let config = Config::ask_new();
        config.export_config("./config.toml".to_string());
        println!("created new config");
        config
    });
    let args: Vec<String> = std::env::args().collect();
    match args[1].as_str() {
        "add-game" => cli::add_games(args, &config),
        "fetch" => cli::fetch(args, &config),
        "pull" => cli::pull(args, &config),
        "help" | "--help" | "-h" => {
            println!("[command] {{options}}\n\n[commands]\n-- add-game\n-- fetch\n-- pull\n-- push");
        },
        _ => {
            println!("{:#?}", args);
        }
    }
}
#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use chrono::{DateTime, Utc, NaiveDateTime};
    use smee2_lib::{game_manager::GameSave, transfer::GameSavePull};
    fn new_game_save() -> GameSave {
        let data = HashMap::new();
        GameSave { game_name: "Test Game".to_string(), 
        save_name: "Test Save".to_string(), 
        time: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc), 
        data: data
        }
    }
    //#[test]
    fn push_pull() {
        let save = new_game_save();
        //let key = smee2_lib::encryption::make_key(vec![0, 1]);
        let key = vec![104, 101, 108, 108, 115, 115, 115, 115, 104, 101, 108, 108, 115, 115, 115, 115, 104, 101, 108, 108, 115, 115, 115, 115, 104, 101, 108, 108, 115, 115, 115, 115];
        if let Err(e) = smee2_client::transfer::push(&save, "127.0.0.1:2048", &key) {
            panic!("test fail push\n{}", e);
        }
        let game_save = GameSavePull {
            game_name: save.game_name.clone(),
            save_name: save.save_name.clone()
        };
        match smee2_client::transfer::pull("127.0.0.1:2048", &key, game_save) {
            Ok(downloaded_save) => {
                if downloaded_save != save {
                    panic!("DATA LOST!!")
                }
            }
            Err(e) => panic!("test fail pull\n{}", e),
        }
    }
}