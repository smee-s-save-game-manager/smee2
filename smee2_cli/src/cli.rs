use smee2_client::Config;
use smee2_lib::transfer::GameSavePull;
use std::collections::HashMap;
use std::format;
use smee2_client::{game_profile, transfer};
pub fn pull(args: Vec<String>, config: &Config) {
    let game_name: String;
    let term = console::Term::stdout();
    if args.len() >= 2 {
        game_name = args[1].clone();
    } else {
        game_name = term.read_line_initial_text("Game Name:").unwrap();
    }
    let (mut profiles, _, bad_files, _) = game_profile::get_profiles(&config.profile_path);
    if bad_files.len() != 0 {
        for file in bad_files {
            println!("can't open file: {}", file);
        }
    }
    let save_name = term.read_line_initial_text("Game Save:").unwrap();
    let mut game_save = GameSavePull::latest(&game_name);
    if save_name != "".to_string() {
        game_save.save_name = save_name;
    }
    profiles.retain(|x| &x.name == &game_name);
    match profiles.len() {
        0 => {
            println!("Not in profile, add game");
        },
        1 => {
            if let Ok(save) = transfer::pull(&config.server_address, &config.key, game_save) {
                game_profile::restore(&profiles[0], save);
            } else {
                println!("Error pulling save from server");
            }
        }
        _ => {
            println!("Conflicting Profiles with the same name");
        }
    }
    
}
#[allow(unused_variables)]
pub fn fetch(args: Vec<String>, config: &Config) {

}

pub fn add_games(args: Vec<String>, config: &Config) {
    if args.len() == 1 {
        let term = console::Term::stdout();
        if let Ok(game_name) = term.read_line_initial_text("Game Name:") {
            let mut map: HashMap<String, (String, bool)> = HashMap::new();
            let _ = term.write_line("paths to backup, seperate with \",\"");
            if let Ok(saves) = term.read_line() {
                for path in saves.split(", ") {
                    let path_buf = std::path::Path::new(path);
                    if path_buf.exists() {
                        map.insert(path.to_owned(), (path.to_owned(), path_buf.metadata().unwrap().is_dir()));
                    } else {
                        let _ = term.write_line(format!("can't use path: {}", path).as_str());
                        return;
                    }
                }
                let profile = smee2_lib::game_manager::GameProfile {
                    name: game_name,
                    saves: map
                };
                if let Err(e) = game_profile::create_profile(&config.profile_path, profile) {
                    println!("{:#?}", e);
                }
            }
        }
    }
}