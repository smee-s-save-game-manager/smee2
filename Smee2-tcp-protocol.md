# Smee2 TCP protocol

---

## fetch

```mermaid
sequenceDiagram
Client-->>Server: connection
note right of Server: accept
note over Client, Server: encrypted
rect rgb(0, 0, 200)
Client->>Server: "fetch"
Client->>Server: game name
Server->>Client: save name + time
end
```

## fetch games

```mermaid
sequenceDiagram
Client-->>Server: connection
note right of Server: accept
note over Client, Server: encrypted
rect rgb(0, 0, 200)
Client->>Server: "fetch games"
Server->>Client: game names
end
```

## fetch save location names

```mermaid
sequenceDiagram
Client-->>Server: connection
note right of Server: accept
note over Client, Server: encrypted
rect rgb(0, 0, 200)
Client->>Server: "Fetch Save Location Names"
Client->>Server: GameSavePull [game name (optional, specific save name)]
Server->>Client: save location names

end
```

## pull

```mermaid
sequenceDiagram
Client-->>Server: connection
note right of Server: accept
note over Client, Server: encrypted
rect rgb(0, 0, 200)
Client->>Server: "pull"
Client->>Server: GameSavePull [game name (optional, specific save name)]
Server->>Client: save game object

end
```

## push

```mermaid
sequenceDiagram
Client-->>Server: connection
note right of Server: accept
note over Client, Server: encrypted
rect rgb(0, 0, 200)
Client->>Server: "push" 
Client->>Server: game save object
end
```
