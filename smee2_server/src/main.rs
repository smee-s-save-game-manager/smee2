use std::{net::{TcpListener}, fs::{create_dir}, path::Path};
use smee2_lib::{transfer::{SmeeStream, CallType, Encode, GameSavePull}, game_manager::GameSave};
mod fs;
mod config;
fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() >= 2 {
        match args[1].as_str() {
            "-h" | "--help" | "help" => {
                println!("OPTIONS\nmake-config [-c]")
            },
            "-c" | "--make_config" | "make_config" => {
                let term = console::Term::stdout();
                println!("TCP Port:");
                let port_opt = term.read_line();
                println!("Source Directory:");
                let source_opt = term.read_line();
                println!("Encryption key:");
                let key_opt = term.read_line();
                if let (Ok(port), Ok(source), Ok(key_str)) = (port_opt, source_opt, key_opt) {
                    let key: [u8; 32] = smee2_lib::encryption::make_key(key_str.as_bytes().to_vec());
                    let conf = config::Config::new(key, source, port);
                    if let Err(e) = conf.export_config() {
                        println!("Error writing to file :\n{}", e);
                    }
                } else {
                    println!("Error");
                }
            },
            _ => {
    
            }
        }
    }

    let configuration = config::Config::import_config();
    if configuration.is_none() {
        println!("No config file, create it");
        return;
    }
    let configuration = configuration.unwrap();
    let key = configuration.key;
    let source = configuration.source;
    if let Err(e) = std::fs::create_dir_all(&source) {
        panic!("Can't make source folder: {}\nProbably a permission error", e);
    }
    let listener = TcpListener::bind("127.0.0.1:".to_string() + &configuration.port);
    if let Err(e) = listener {
        panic!("can't host! {}", e)
    }
    let listener = listener.unwrap();
    // accept connections and process them serially
    for stream in listener.incoming() {
        if let Err(e) = &stream {
            println!("{}", e);
            return;
        }
        let stream = stream.unwrap();
        let mut stream = SmeeStream::new(stream);
        if let Some(call) = stream.smee_read::<CallType>(&key) {
            match call {
                CallType::Fetch => fetch(stream, &key, &source),
                CallType::FetchGames => fetch_games(stream, &key, &source),
                CallType::FetchSaveLocationNames => fetch_save_location_names(stream, &key, &source),
                CallType::Pull => pull(stream, &key, &source),
                CallType::Push => push(stream, &key, &source)
            }
        }
    }
}

fn get_games(source: &str) -> Vec<String> {
    let games = match fs::get_games(source) {
        Ok(games) => games,
        Err(e) => {
            println!("Error trying to read games on server {:#?}", e);
            Vec::new()
        }
    };
    games
}

fn get_saves(game_path: &str) -> Vec<String> {
    let saves = match fs::get_saves(game_path) {
        Ok(saves) => saves,
        Err(e) => {
            println!("Error trying to read games on server {:#?}", e);
            Vec::new()
        }
    };
    saves
}

fn fetch(mut stream: SmeeStream, key: &[u8], source: &str) {
    let game_name = stream.smee_read::<String>(key);
    if game_name.is_none() {
        return;
    }
    let game_name = game_name.unwrap();
    //find game name in database
    let games = get_games(source);
    if games.contains(&game_name) {
        let source_path = Path::new(source);
        let game_path_ = source_path.join(&game_name);
        let game_path = game_path_.to_str().unwrap();
        let saves = get_saves(game_path);
        stream.smee_write(&saves, key);
    }
}

fn fetch_games(mut stream: SmeeStream, key: &[u8], source: &str) {
    //send database
    let games = get_games(source);
    stream.smee_write(&games, key);
}

fn pull(mut stream: SmeeStream, key: &[u8], source: &str) {
    //push SaveGame to client
    let games = get_games(source);
    let save = stream.smee_read::<GameSavePull>(key);
    if save.is_none() {
        return;
    }
    let save = save.unwrap();
    if !games.contains(&save.game_name) {
        return;
    }
    if save.save_name == smee2_lib::transfer::SaveType::Latest {
        //todo: Add support for latest
        unimplemented!("'Latest' is not yet supported")
    }
    if let smee2_lib::transfer::SaveType::Specific(save_name) = &save.save_name {
        let source_path = Path::new(source);
        let game_path_ = source_path.join(&save.game_name);
        let game_path = game_path_.to_str().unwrap();
        if !get_saves(game_path).contains(save_name) {
            return;
        }
        let save_path_ = game_path_.join(save_name);
        let save_path = save_path_.to_str().unwrap();
        match std::fs::read(save_path) {
            Err(e) => println!("{}", e),
            Ok(encoded) => {
                match GameSave::decode(&encoded) {
                    Err(e) => println!("Error in decoding \n{}", e),
                    Ok(save) => {
                        stream.smee_write(&save, key);
                    }
                }
            }
        }
    }
}

fn push(mut stream: SmeeStream, key: &[u8], source: &str) {
    //pull SaveGame from client
    let source_path = Path::new(source);
    let game = stream.smee_read::<GameSave>(key);
    if game.is_none() {
        return;
    }
    let game = game.unwrap();
    let game_path_ = source_path.join(&game.game_name);
    let game_path = game_path_.to_str().unwrap();
    let save_path_ = game_path_.join(&game.save_name);
    let save_path = save_path_.to_str().unwrap();
    if get_games(source).contains(&game.game_name) {
        if !get_saves(game_path).contains(&game.save_name) {
            match game.encode() {
                Err(e) => println!("{}", e),
                Ok(encoded) => {
                    if let Err(e) = fs::create_file(save_path, encoded) {
                        println!("Can't save to file {}", e);
                    }
                }
            }
        }
    } else {
        match create_dir(game_path) {
            Err(e) => println!("{}", e),
            Ok(()) => {
                match game.encode() {
                    Err(e) => println!("{}", e),
                    Ok(encoded) => {
                        if let Err(e) = fs::create_file(save_path, encoded) {
                            println!("{}", e);
                        }
                    }
                }
            }
        }
    }
}

fn fetch_save_location_names(mut stream: SmeeStream, key: &[u8], source: &str) {
    let games = get_games(source);
    let save = stream.smee_read::<GameSavePull>(key);
    if save.is_none() {
        return;
    }
    let save = save.unwrap();
    if games.contains(&save.game_name) {
        if save.save_name == smee2_lib::transfer::SaveType::Latest {
            //todo: Add support for latest
            unimplemented!("\'Latest\' is not yet supported")
        }
        let source_path = Path::new(source);
        let game_path_ = source_path.join(&save.game_name);
        let game_path = game_path_.to_str().unwrap();
        if let smee2_lib::transfer::SaveType::Specific(save_name) = &save.save_name {
            if get_saves(game_path).contains(save_name) {
                let save_path_ = game_path_.join(save_name);
                let save_path = save_path_.to_str().unwrap();
                match std::fs::read(save_path) {
                    Err(e) => println!("{}", e),
                    Ok(encoded) => {
                        match GameSave::decode(&encoded) {
                            Err(e) => println!("{}", e),
                            Ok(save) => {
                                let save_location_names: Vec<String> = save.data.keys().map(|x| x.to_owned()).collect();
                                stream.smee_write(&save_location_names, key);
                            }
                        }
                    }
                }
            }
        }
    }
}