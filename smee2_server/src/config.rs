use serde::{Serialize, Deserialize};
use std::{fs::{read_to_string, write}};

#[derive(Serialize, Deserialize)]
pub struct Config {
	pub key: [u8; 32],
	pub source: String,
	pub port: String
}

impl Config {
    pub fn new(key: [u8; 32], source: String, port: String) -> Config {
        Config {
            key,
            source,
            port
        }
    }
	pub fn import_config() -> Option<Config>{
		match read_to_string("./server_config.toml") {
            Ok(config_str) => {
                if let Ok(config) = toml::from_str(&config_str) {
                    Some(config)
                } else {
                    None
                }
            },
            Err(e) => {
                if e.kind() != std::io::ErrorKind::NotFound {
                    println!("can't read config file {:#?}", e);
                }
                None
            }
        }
	}
    pub fn export_config(&self) -> Result<(), Box<dyn std::error::Error>> {
        match toml::to_string(self) {
            Ok(conf) => {
                match write("./server_config.toml", conf) {
                    Ok(()) => {
                        Ok(())
                    },
                    Err(e) => {
                        let e: Box<dyn std::error::Error> = Box::new(e);
                        Err(e)
                    }
                }
            },
            Err(e) => {
                let e: Box<dyn std::error::Error> = Box::new(e);
                Err(e)
            }
        }
    }
}