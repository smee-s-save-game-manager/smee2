use std::fs::read_dir;
use std::io::{Error, Write, ErrorKind};
use std::fs::File;

/// create new file with binary data
pub fn create_file(source: &str, contents: Vec<u8>) -> Result<(), Error>{
	let mut file = File::create(source)?;
    file.write_all(&contents)
}

/// find out what saves are stored in server
pub fn get_saves(source: &str) -> Result<Vec<String>, Error> {
	let mut games = Vec::<String>::new();
    let source_dir = read_dir(source)?;
    for entry in source_dir {
        let entry = entry?;
        if !entry.path().is_dir() {
            if let Ok(dir_name) = entry.file_name().into_string() {
                games.push(dir_name);
            } else {
                return Err(
                    Error::new(
                        ErrorKind::Other, 
                        "Can't convert type OsString to String"
                    ));
            }
        }
    }
    Ok(games)
}

/// find out what games have save data stored on server
pub fn get_games(source: &str) -> Result<Vec<String>, Error> {
	let mut games = Vec::<String>::new();
    let source_dir = read_dir(source)?;
    for entry in source_dir {
        let entry = entry?;
        if entry.path().is_dir() {
            match entry.file_name().into_string() {
                Err(_e) => return Err(Error::new(std::io::ErrorKind::Other, "Can't convert OsString name")),
                Ok(dir_name) => {games.push(dir_name);},
            }
        }
    }
	Ok(games)
}