use std::{path::Path};
use std::io::Error;
use smee2_lib::game_manager::{GameProfile};
use crate::fs;
use smee2_lib::compression::{decompress, compress, tarball, un_tarball};
use smee2_lib::game_manager::GameSave;
use std::collections::HashMap;
/// read profiles from directory
pub fn get_profiles<P: AsRef<std::path::Path>>(dir_path: P) -> Result<(Vec<GameProfile>, Vec<toml::de::Error>), std::io::Error> {
	let mut profiles = Vec::<GameProfile>::new();
	let mut deserialize_errs = Vec::<toml::de::Error>::new();
	let files = fs::dir_read(dir_path)?;
	for file in files {
		match toml::from_str(&file) {
			Ok(n) => {
				profiles.push(n);
			},
			Err(e) => {
				deserialize_errs.push(e)
			}
		}
		
	}
	Ok((profiles, deserialize_errs))
}

/// write out [GameProfile] to file
pub fn create_profile<P: AsRef<std::path::Path> + std::convert::AsRef<std::ffi::OsStr>>(dir_path: P, profile: GameProfile) -> Result<(), Error> {
	let path = Path::new(&dir_path).join(profile.name.to_string() + ".toml");
	if let Ok(contents) = toml::to_string(&profile) {
		std::fs::write(path, contents)
	} else {
		Err(std::io::Error::new(std::io::ErrorKind::Other, "can't make file"))
	}
}

/// Backup file into [GameSave] object
/// uses [smee2_lib::compression::compress] to compress file
pub fn backup(profile: GameProfile, save_name: String, time: chrono::DateTime<chrono::Utc>,) -> Result<GameSave, Error> {
	let mut data: HashMap<String, (Vec<u8>, bool)> = HashMap::new();
	for path_str in profile.saves {
		let path = Path::new(&path_str.1.0);
		if !path_str.1.1 {
			//is file
			let file = std::fs::File::open(path)?;
			let compressed: Vec<u8> = Vec::new();
			let compressed = compress(file, compressed)?;
			data.insert(path_str.0, (compressed, path_str.1.1));
		} else {
			//is dir
			let tarballed = tarball(path_str.1.0.as_str())?;
			let compressed: Vec<u8> = Vec::new();
			let compressed = compress(tarballed.as_slice(), compressed)?;
			data.insert(path_str.0, (compressed, path_str.1.1));
		}
	}
	let save = GameSave {
		game_name: profile.name,
		save_name, 
		time, 
		data,
	};
	Ok(save)
}


/// Restore [GameSave] object, using local [GameProfile] to know where to store files
pub fn restore(profile: &GameProfile, game_save: GameSave) -> Result<(), Error>{
	let save_datas = game_save.data;
	for save in &profile.saves {
		let save_location_name = save.0;
		let save_location = save.1.0.to_owned();
		let is_dir = save.1.1;
		match save_datas.get(&save_location_name.to_owned()) {
			None => {
				//return Some(Error::new(std::io::ErrorKind::Other, format!("Can't find {} in game save", save_location_name)));
				// assume it's a new location
				println!("Can't find {} in game save", save_location_name);
			},
			Some(compressed_save_data) => {
				let mut save_data: Vec<u8> = Vec::new();
				let _ = decompress(compressed_save_data.0.as_slice(), &mut save_data)?;
				if is_dir {
					un_tarball(save_data, save_location.as_str())?;
				} else {
					std::fs::write(save_location, save_data)?;
				}
			}
		}
	}
	Ok(())
}