use std::{path::Path, ffi::OsStr};

/// read files from directory
pub fn dir_read<P: AsRef<Path>>(dir_path: P) -> Result<Vec<String>, std::io::Error> {
	let mut files = Vec::<String>::new();
	let dir = std::fs::read_dir(dir_path)?;
	for entry in dir {
		let file = entry?;
		if let Ok(filetype) = file.file_type() {
			if filetype.is_dir() {
				let mut files2 = dir_read(file.path())?;
				files.append(&mut files2);
			} else if file.path().extension() == Some(OsStr::new("toml")) {
				files.push(std::fs::read_to_string(file.path())?);
			}
		}
	}
	Ok(files)
}