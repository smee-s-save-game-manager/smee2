use std::{fs::read_to_string};
pub mod transfer;
pub mod fs;
pub mod game_profile;
use std::io::ErrorKind;
use serde::{Serialize, Deserialize};
use chrono::{DateTime, Utc};

pub fn current_time() -> DateTime<Utc> {
    Utc::now()
}

/// client config file
#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub profile_path: String,
    pub key: [u8; 32],
    pub server_address: String
}

impl Config {
    //for python
    pub fn toml_from_str(config_str: &str) -> Config {
        toml::from_str(config_str).unwrap()
    }
    /// read config from file
    pub fn import_config(config_file: String) -> Option<Config> {
        match read_to_string(config_file) {
            Ok(config_str) => {
                if let Ok(config) = toml::from_str(&config_str) {
                    Some(config)
                } else {
                    None
                }
            },
            Err(e) => {
                if e.kind() != std::io::ErrorKind::NotFound {
                    println!("can't read config file {:#?}", e);
                }
                None
            }
        }
    }
    /// make new config, ensuring key is correct
    pub fn new(profile_path: String, key_str: String, server_address: String) -> Config {
        let key = smee2_lib::encryption::make_key(key_str.into_bytes());
        Config { 
            profile_path, 
            key, 
            server_address 
        }
    }
    //todo: remove
    /// ask for config with CLI
    pub fn ask_new() -> Config {
        if let Some(config_path) = dirs::config_dir() {
            let profile_path = config_path.join("smee2");
            let term = console::Term::stdout();
            match term.read_line_initial_text("Encryption Key:\n") {
                Ok(key_str) => {
                    let key_str = key_str.strip_prefix("Encryption Key:\n").unwrap_or("").to_string();
                    let key = smee2_lib::encryption::make_key(key_str.into_bytes());
                    if let Err(e) = std::fs::create_dir(&profile_path) {
                        match e.kind() {
                            ErrorKind::PermissionDenied => {
                                println!("Permission Denied {}", profile_path.display());
                            },
                            ErrorKind::AlreadyExists => {
        
                            },
                            _ => {
                                println!("can't create config directory: {:#?}", e);
                            }
        
                        }
                    }
                    let server_address: String = { 
                        if let Ok(input) = term.read_line_initial_text("Server Address\n") {
                            if let Some(s) = input.strip_prefix("Server Address\n") {
                                s.to_string();
                            }
                        }
                        "".to_string()
                    };
                    
                    return Config { 
                        profile_path: profile_path.to_str().unwrap().to_string(),
                        server_address,
                        key
                    };
                },
                Err(e) => {
                    println!("Can't create encryption key {:#?}", e);
                }
                
            }
        }
        panic!("Can't access config directory");
    }
    pub fn export_config(&self, config_file: String) {
        if let Ok(config_str) = toml::to_string(&self) {
            if let Err(e) = std::fs::write(config_file, config_str) {
                println!("can't export config: {:#?}", e);
            }
        }
    }
}