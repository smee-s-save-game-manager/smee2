use std::{net::TcpStream};
use smee2_lib::{game_manager::GameSave, transfer::{SmeeStream, CallType, GameSavePull}};
use std::io::Error;

/// Ask what are the available saves of game
pub fn fetch(server_address: &str, key: &[u8], game_name: String) -> Result<Vec<String>, Error> {
	let tcp_stream = TcpStream::connect(server_address)?;
	let mut stream = SmeeStream::new(tcp_stream);
	//call for fetch
	stream.smee_write(&CallType::Fetch, key);
	//send game save fetch
	stream.smee_write(&game_name, key);
	//read saves
	match stream.smee_read::<Vec<String>>(key) {
		Some(saves) => Ok(saves),
		None => Err(std::io::Error::new(std::io::ErrorKind::NotFound, "Can't download data from smee server"))
	}
}

/// ask what games are available
pub fn fetch_games(server_address: &str, key: &[u8]) -> Result<Vec<String>, Error> {
	let tcp_stream = TcpStream::connect(server_address)?;
	let mut stream = SmeeStream::new(tcp_stream);
	//call for fetch games
	stream.smee_write(&CallType::FetchGames, key);
	//recieve data
	match stream.smee_read::<Vec<String>>(key) {
		Some(games) => Ok(games),
		None => Err(std::io::Error::new(std::io::ErrorKind::NotFound, "Can't download data from smee server"))
	}
}

/// upload new save game
pub fn push(save: &GameSave, server_address: &str, key: &[u8]) -> Result<(), Error> {
	let tcp_stream = TcpStream::connect(server_address)?;
	let mut stream = SmeeStream::new(tcp_stream);
	//call for push
	stream.smee_write(&CallType::Push, key);
	//push
	stream.smee_write(&save, key);
	Ok(())
}

/// download old save game
pub fn pull(server_address: &str, key: &[u8], game_save: GameSavePull) -> Result<GameSave, Error>{
	let tcp_stream = TcpStream::connect(server_address)?;
	let mut stream = SmeeStream::new(tcp_stream);
	//call for pull
	stream.smee_write(&CallType::Pull, key);
	//send game save pull
	stream.smee_write(&game_save, key);
	//read gamesave
	match stream.smee_read::<&GameSave>(key) {
		Some(save) => Ok(save),
		None => Err(std::io::Error::new(std::io::ErrorKind::NotFound, "Can't download data from smee server"))
	}
}

/// Ask what save locations are in a game save
pub fn fetch_save_location_names(server_address: &str, key: &[u8], game_save: GameSavePull) -> Result<Vec<String>, Error> {
	let tcp_stream = TcpStream::connect(server_address)?;
	let mut stream = SmeeStream::new(tcp_stream);
	//call for pull
	stream.smee_write(&CallType::FetchSaveLocationNames, key);
	//send game save pull
	stream.smee_write(&game_save, key);
	//read gamesave
	match stream.smee_read::<Vec<String>>(key) {
		Some(save) => Ok(save),
		None => Err(std::io::Error::new(std::io::ErrorKind::NotFound, "Can't download data from smee server"))
	}
}