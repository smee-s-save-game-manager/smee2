use lz4::{Decoder, EncoderBuilder};
use std::io::{Read, Write, Error};
use std::io;

/// Compresses data with [lz4]
pub fn compress<R: Read, W: Write>(mut source: R, destination: W) -> Result<W, Error> {
	let mut encoder = EncoderBuilder::new()
		.level(5)
		.build(destination)?;
	io::copy(&mut source, &mut encoder)?;
	let (output, result) = encoder.finish();
	match result {
		Ok(()) => Ok(output),
		Err(e) => Err(e)
	}
}

/// Decompresses data with [lz4]
pub fn decompress<R: Read, W: Write>(source: R, mut destination: W) -> io::Result<u64> {
    let mut decoder = Decoder::new(source)?;
    io::copy(&mut decoder, &mut destination)
}

/// Creates a tarball of a directory, returns binary
pub fn tarball(dir_path: &str) -> Result<Vec<u8>, Error> {
	let buffer = Vec::<u8>::new();
	let mut tar_builder = tar::Builder::new(buffer);
	tar_builder.append_dir_all(".", dir_path)?;
	tar_builder.into_inner()
}

/// Extracts tarball to destination directorty
pub fn un_tarball(tarball: Vec<u8>, destination: &str) -> io::Result<()> {
	let mut archive = tar::Archive::new(tarball.as_slice());
	std::fs::remove_dir_all(destination)?;
	archive.unpack(destination)
}