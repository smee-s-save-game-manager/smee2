use serde::{Serialize, Deserialize};
use std::{net::TcpStream, io::{Write, Read}, collections::HashMap};
use crate::{game_manager::{GameSave}, encryption};

/// Types of calls the client can make to the server
pub enum CallType {
	/// Fetch all saves of a game 
	Fetch,
	/// Fetch all games
	FetchGames,
	/// Fetch names of save locations of given save
	FetchSaveLocationNames,
	/// Download save
	Pull,
	/// Upload save
	Push
}

impl CallType {
	/// Translates call back from u8 after transfer
	pub fn from_u8(input: u8) -> CallType {
		match input {
			0 => Self::Fetch,
			2 => Self::Pull,
			3 => Self::Push,
			4 => Self::FetchSaveLocationNames,
			_ => Self::FetchGames
		}
	}
	/// Translates call to u8 for transfer
	pub fn to_u8(&self) -> u8 {
		match &self {
			Self::Fetch => 0,
			Self::Pull => 2,
			Self::Push => 3,
			Self::FetchSaveLocationNames => 4,
			_ => 1		
		}
	}
}

/// Type for pull demands
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GameSavePull {
	pub game_name: String, 
	pub save_name: SaveType,
}
//todo: use enum instead
impl GameSavePull {
	/// to ask for the latest save
	pub fn latest(game_name: &str) -> GameSavePull {
		GameSavePull {
			game_name: String::from(game_name), 
			save_name: SaveType::Latest
		}
	}
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum SaveType {
	Latest,
	Specific(String)
}

/// methods to encode and decode data types for transfer
pub trait Encode {
	type Input;
	/// Encode data using [bincode] for transport 
	fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>>;
	/// Decode data using [bincode] after transport 
	fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>>;
}

//defaut implementation of Encode trait
macro_rules! impl_encode {
	($encodetype:ty) => {
		impl Encode for $encodetype {
			type Input = $encodetype;
			fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
				bincode::serialize(&self)
			}
			fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
				bincode::deserialize(encoded)
			}
		}
	};
	($encodetype:ty, $decodetype:ty) => {
		impl Encode for $encodetype {
			type Input = $decodetype;
			fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
				bincode::serialize(&self)
			}
			fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
				bincode::deserialize(encoded)
			}
		}
	};
}

impl_encode!(GameSave);
impl_encode!(&GameSave, GameSave);
impl_encode!(GameSavePull);
impl_encode!(String);
impl_encode!(Vec<String>);
impl_encode!(HashMap<String, String>);

impl Encode for CallType {
	type Input = CallType;
	fn encode(&self) -> Result<Vec<u8>, Box<bincode::ErrorKind>> {
		bincode::serialize(&self.to_u8())
	}
	fn decode(encoded: &[u8]) -> Result<Self::Input, Box<bincode::ErrorKind>> {
		let decode = bincode::deserialize(encoded)?;
		Ok(CallType::from_u8(decode))
	}
}

/// abstraction for TCP protocol
pub struct SmeeStream(TcpStream);

impl SmeeStream {
	pub fn new(stream: TcpStream) -> SmeeStream {
		SmeeStream(stream)
	}
	/// tcp write, but first sends the amount of bytes
	fn send(&mut self, buf: &[u8]) -> Result<(), std::io::Error> {
		self.0.write_all(&(buf.len() as u64).to_be_bytes())?;
		self.0.write_all(buf)
	}
	/// tcp read, but knows the amount of data to read
	fn receive(&mut self) -> Result<Vec<u8>, std::io::Error> {
		let mut size_buf: [u8; 8] = [0; 8]; //u64
		self.0.read_exact(&mut size_buf)?;
		let length = u64::from_be_bytes(size_buf);
		let mut buf = Vec::<u8>::new();
		for _x in 0..(length as usize) {
			buf.push(0);
		}
		self.0.read_exact(&mut buf)?;
		Ok(buf)
	}
	/// Send data
	pub fn smee_write<T: Encode>(&mut self, data: &T, key: &[u8]) -> Option<()>{
		match data.encode() {
			Ok(encoded) => {
				match encryption::encrypt(key, &encoded) {
					Ok(encrypted) => {
						match self.send(&encrypted) {
							Ok(()) => Some(()),
							Err(e) => {
								println!("smee write uploading fail: {}", e);
								None
							}
						}
					},
					Err(e) => {
						println!("smee write encrypting fail: {}", e);
						None
					}
				}
			},
			Err(e) => {
				println!("smee write encoding fail: {}", e);
				None
			}
		}
	}
	/// Read data
	pub fn smee_read<T: Encode>(&mut self, key: &[u8]) -> Option<T::Input> {
		match self.receive() {
			Ok(buf) => {
				match encryption::decrypt(key, &buf) {
					Ok(decrypted) => {
						match T::decode(&decrypted) {
							Ok(output) => Some(output),
							Err(e) => {
								println!("smee read decoding error {}", e);
								None
							}
						}
					},
					Err(e) => {
						println!("smee read decryption error {}", e);
						None
					}
				}
			},
			Err(e) => {
				println!("smee read download error: {}", e);
				None
			}
		}
	}
}