use aes_gcm_siv::{
    aead::{Aead, KeyInit},
    Aes256GcmSiv, Nonce // Or `Aes128GcmSiv`
};
use chrono::{Duration, Utc};
/// nonces are made from the universal time, this can cause a problem because the time it is created at and the time it is used for decryption can be different, 
/// so first we try the current time, then we try with an hour ago
/// if now is true, then it creates the nonce of the current time, if false, the last nonce
fn get_nonce(now: bool) -> Vec<u8> {
	//based on time, assume if it doesn't work that it's because it was sent the previous hour
	let today = match now {
		true => Utc::today(),
		false => Utc::today() - Duration::days(1) //yesterday
	};
	let mut vec = today.to_string().as_bytes().to_vec();
	//to make it the correct size, don't understand but it works, requires a less dumb answer
	for _x in  0..(vec.len() - 12) {
		vec.remove(0);
	}
	vec
}

/// Makes a valid 256 bit encryption key from any [Vec<u8>] u8
pub fn make_key(key: Vec<u8>) -> [u8; 256/8] {
	let mut real_key: [u8; 256/8] = [0; 256/8];
	let input_size = key.len();
	for i in 0..real_key.len() {
		real_key[i] = key[i % input_size];
	}
	real_key
}


/// This is the actual encryption function, uses a key made with the [make_key] and any binary data
/// automatically makes a nonce based on current time
pub fn encrypt(key: &[u8], plaintext: &[u8]) -> Result<Vec<u8>, aes_gcm_siv::Error> {
	let cipher = Aes256GcmSiv::new_from_slice(key).unwrap();
	let nonce_vec = get_nonce(true);
	let nonce = Nonce::from_slice(&nonce_vec);
	cipher.encrypt(nonce, plaintext)
}

/// Decrypts data, tries with current nonce, if it doesn't work, tries again with the last nonce
pub fn decrypt(key: &[u8], ciphertext: &[u8]) -> Result<Vec<u8>, String> {
	match Aes256GcmSiv::new_from_slice(key) {
		Ok(cipher) => {
			let mut nonce_vec = get_nonce(true);
			let mut nonce = Nonce::from_slice(&nonce_vec);
			match cipher.decrypt(nonce, ciphertext) {
				Ok(plaintext) => Ok(plaintext),
				Err(e) => {
					println!("decryption error {}", e);
					nonce_vec = get_nonce(false);
					nonce = Nonce::from_slice(nonce_vec.as_ref());
					match cipher.decrypt(nonce, ciphertext) {
						Ok(plaintext) => Ok(plaintext),
						Err(e) => {
							println!("decryption error after trying yesterday's nonce {}", e);
							Err(e.to_string())
						}
					}
				}
			}
		},
		Err(e) => {
			println!("cipher creation error {}", e);
			Err(e.to_string())
		}
	}
}