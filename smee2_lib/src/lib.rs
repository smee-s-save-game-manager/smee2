#![allow(dead_code)]
pub mod compression;
pub mod game_manager;
pub mod encryption;
pub mod transfer;
#[cfg(test)]
mod tests {
    use crate::{encryption::{encrypt, make_key, decrypt}, transfer::{Encode}};

	#[test]
	fn encryption_test() {
		let key = make_key(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
		let plaintext = "message".as_bytes();
		match encrypt(&key, &plaintext) {
			Ok(ciphertext) => {
				match decrypt(&key, &ciphertext) {
					Ok(decrypted) => {
						if decrypted != plaintext {
							panic!("DATA LOST DURING ENCRYPTION");
						}
					},
					Err(e) => {
						panic!("can't decrypt: {}", e);
					}
				}
			},
			Err(e) => {
				panic!("can t encrypt: {}", e);
			}
		}
	}
	#[test]
	fn encoding_test() {
		let message = "message".to_string();
		match message.encode() {
			Ok(encoded) => {
				match <String as crate::transfer::Encode>::decode(&encoded) {
					Ok(decoded) => {
						if message != decoded {
							panic!("failed dencode test: origional: {} \ndecoded: {}", message, decoded);
						}
					},
					Err(e) => {
						panic!("failed encode test: {:?}", e);
					}
				}
			},
			Err(e) => {
				panic!("failed encode test: {:?}", e);
			}
		}
	}
}