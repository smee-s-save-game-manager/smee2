use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use std::collections::HashMap;

/// Object representing a save game backup
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct GameSave {
	pub game_name: String, 
	pub save_name: String, 
	pub time: DateTime<Utc>, 
	/// <Save location name, (save data, is dir?)>
	pub data: HashMap<String, (Vec<u8>, bool)>
}

/// Keeps tract of the paths to data 
#[derive(Serialize, Deserialize, Clone)]
pub struct GameProfile {
	pub name: String,
	/// <Save location name, (save location, is dir?)>
	pub saves:  HashMap<String, (String, bool)>
}